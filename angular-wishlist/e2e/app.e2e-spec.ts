import { AngularWishlistPage } from './app.po';

describe('angular-wishlist App', function() {
  let page: AngularWishlistPage;

  beforeEach(() => {
    page = new AngularWishlistPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
